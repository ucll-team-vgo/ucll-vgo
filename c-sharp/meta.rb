require 'MetaData'
require 'LaTeX'
require 'Image'
require 'Html'
require 'Upload'


class Context
  include Contracts::TypeChecking
end


meta_object do
  extend MetaData::Actions
  extend LaTeX::Actions
  extend Image::Actions
  extend Html::Actions
  extend Upload::Mixin

  def remote_directory
    world.parent.remote_directory + 'csharp-overview'
  end

  image_basenames = [ 'namespaces', 'packages' ]
  tex_filenames = image_basenames.map { |x| "#{x}.tex" }
  png_filenames = image_basenames.map { |x| "#{x}.png" }

  tex_actions(*tex_filenames, group_name: :tex)
  pdf_to_png(*image_basenames, group_name: :images)

  html_template('overview', group_name: 'overview', context: Context.new)
  
  uploadable(*(png_filenames + [ 'overview.html' ]))
  upload_action

  group_action(:full, [:tex, :images, :overview, :upload])
end
