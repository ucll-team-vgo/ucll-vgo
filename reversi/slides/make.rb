#!ruby

ID = 'reversi'

$:.unshift( ENV['KHL_ROOT_DIRECTORY'] + '/shared' )
require 'shared.rb'


commands do
  command 'upload' do
    action do
      remote do |scp|
        scp.upload("#{ID}.pdf", "/var/www/VGO")
      end
    end
  end
end
