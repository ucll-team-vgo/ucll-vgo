#!ruby

path = '.'
while not Dir.exists?( path + '/rubylib' )
  parent = File.expand_path( path + '/..' )
  abort "Could not find rubylib... Aborting!" unless parent != path
  path = parent
end
$:.unshift( path + '/rubylib' )

require 'Make.rb'


def upload
  Remote.upload( 'ai-slides.pdf', '/var/www/VGO/' )
end


def compile
  LaTeX.compile( 'ai-slides.tex' )
end

commands do
  command 'pdf' do
    short_description { 'Generates pdf' }
    action { compile }
  end

  command 'upload' do
    short_description { 'Uploads to alexander' }
    action { compile; upload }
  end
end
