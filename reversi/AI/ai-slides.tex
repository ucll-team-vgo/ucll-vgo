\documentclass{ucll-slides}
\usepackage{fourier}
\usepackage{bbding}
\usepackage{wasysym}
\usepackage{booktabs}
\usepackage{skak}

\usetikzlibrary{calc,shadows,math,arrows.meta}

\title{Reversi AI}
\author{P. Philippaerts, F. Vogels}


\newcommand{\TAG}[1]{{\tiny \hfill git: #1}}
\newcommand{\TITLE}[1]{
  \begin{frame}
    \begin{center}
      \Huge #1
    \end{center}
  \end{frame}
}

\pgfkeys{
  /tikz/.cd,
  black stone/.style={ball color=black,draw},
  white stone/.style={ball color=white,draw},
  next move/.style={black,dashed},
  hilight/.style={draw,red,thick}
}
\newcommand{\BLACK}[1]{\draw[black stone] ($ (#1) + (.5,.5) $) circle (.4);}
\newcommand{\WHITE}[1]{\draw[white stone] ($ (#1) + (.5,.5) $) circle (.4);}
\newcommand{\NEXT}[1]{\draw[next move] ($ (#1) + (.5,.5) $) circle (.4);}
\newcommand{\HILIGHT}[1]{\draw[hilight] ($ (#1) + (.5,.5) $) circle (.45);}


\begin{document}

\begin{frame}
  \titlepage
\end{frame}

\begin{frame}
  \frametitle{Game Tree}
  \structure{Assumptions about the game}
  \begin{itemize}
    \item Two players
    \item Players play in turn: 1-2-1-2-1-2, i.e.\ not 1-1-1-2-2-1-1-2-2
    \item At each turn, the current player must make a choice between a finite number of choices
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Game Tree}
  \begin{center}
    \begin{tikzpicture}[state/.style={circle,draw,font=\tiny,inner sep=0mm,minimum size=.25cm},arc/.style={-latex}]
      \node[state] (s1) {1};

      \visible<2->{
        \foreach[count=\i] \dx in {-3,0,3} {
          \node[state] (s1-\i) at ($ (s1) + (\dx,-1) $) {2};
          \draw[arc] (s1) -- (s1-\i);

          \visible<3->{
            \foreach[count=\j] \dxx in {-1,0,1} {
              \node[state] (s1-\i-\j) at ($ (s1-\i) + (\dxx,-1) $) {1};
              \draw[arc] (s1-\i) -- (s1-\i-\j);

              \visible<4->{
                \foreach[count=\k] \dxxx in {-.25,.25} {
                  \node[state] (s1-\i-\j-\k) at ($ (s1-\i-\j) + (\dxxx,-1) $) {2};
                  \draw[arc] (s1-\i-\j) -- (s1-\i-\j-\k);

                  \visible<5->{
                    \node[anchor=north] (s1-\i-\j-\k-) at (s1-\i-\j-\k.south) { $\vdots$ };
                  }
                }
              }
            }
          }
        }
      }
    \end{tikzpicture}
  \end{center}
  \begin{overprint}
    \onslide<1>
    The root node represents the initial state of the board, i.e.\ no player has made a move yet.

    \onslide<2>
    Say player 1 has three possible moves: each move leads to a different board state.

    \onslide<3>
    Now, let's say player 2 can always react in 3 ways.

    \onslide<4>
    It's player 1's turn again. According to the tree, he has only 2 possible moves for every possible game state.

    \onslide<5>
    The tree can be many, many levels deep. The leaves are end-of-game states.
  \end{overprint}
\end{frame}

\begin{frame}
  \frametitle{Game Tree}
  \begin{itemize}
    \item Every level of the tree corresponds to a turn
    \item The branching factor is determined by how many possible moves there are
    \item One game corresponds by a path starting at the root and going down until a leaf is reached
    \item The game tree contains \emph{all} possible games
    \item A game tree can have infinite depth (e.g.\ chess)
    \item A game tree is \emph{huge} and generally only exists conceptually, will never actually be built in memory
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Using the Game Tree}
  \begin{overprint}
    \onslide<1>
    \begin{itemize}
      \item Consider game with exactly three moves in total
      \item Green: player 1 wins
      \item Blue: player 2 wins
      \item Gray: tie
    \end{itemize}

    \onslide<2>
    \begin{itemize}
      \item Move 1: 3 wins for P1, 2 wins for P2, 1 tie
      \item Move 2: 5 wins for P1, 1 wins for P2
      \item Move 3: 4 wins for P1, 1 wins for P2, 1 tie
    \end{itemize}

    \onslide<3>
    Table for player 1:
    \begin{center}
      \begin{tabular}{ccccc}
        \textbf{Move} & \textbf{Wins} & \textbf{Losses} & \textbf{Ties} & \textbf{Win\%} \\
        \toprule
        1 & 3 & 2 & 1 & 50\% \\
        2 & 5 & 1 & 0 & \color{red} 83\% \\
        3 & 4 & 1 & 1 & 66\% \\
      \end{tabular}
    \end{center}

    \onslide<4>
    Table for player 2:
    \begin{center}
      \begin{tabular}{ccccc}
        \textbf{Move} & \textbf{Wins} & \textbf{Losses} & \textbf{Ties} & \textbf{Win\%} \\
        \toprule
        1 & 2 & 3 & 1 & \color{red} 33\% \\
        2 & 1 & 5 & 0 & 16\% \\
        3 & 1 & 4 & 1 & 16\% \\
      \end{tabular}
    \end{center}

  \end{overprint}

  \begin{center}
    \begin{tikzpicture}[state/.style={circle,draw,font=\tiny,inner sep=0mm,minimum size=.25cm},arc/.style={-latex}]
      \pgfmathsetseed{2}
      \pgfmathdeclarerandomlist{colors}{{blue}{blue}{green}{green}{green}{gray}}

      \node[state] (s1) {};

      \foreach[count=\i] \dx in {-3,0,3} {
        \node[state] (s1-\i) at ($ (s1) + (\dx,-1) $) {};


        \foreach[count=\j] \dxx in {-1,0,1} {
          \node[state] (s1-\i-\j) at ($ (s1-\i) + (\dxx,-1) $) {};
          \draw[arc] (s1-\i) -- (s1-\i-\j);

          \foreach[count=\k] \dxxx in {-.25,.25} {
            \pgfmathrandomitem{\c}{colors}
            \node[state,fill=\c] (s1-\i-\j-\k) at ($ (s1-\i-\j) + (\dxxx,-1) $) {};
            \draw[arc] (s1-\i-\j) -- (s1-\i-\j-\k);
          }
        }
      }

      \draw[arc] (s1) -- (s1-1) node[midway,sloped,above,font=\tiny] {1};
      \draw[arc] (s1) -- (s1-2) node[midway,left,font=\tiny] {2};
      \draw[arc] (s1) -- (s1-3) node[midway,sloped,above,font=\tiny] {3};      
    \end{tikzpicture}
  \end{center}
\end{frame}

\begin{frame}
  \frametitle{Artificial Intelligence}
  \structure{Proposed Algorithm}
  \begin{itemize}
    \item Pick the move that leads to the greatest win\%.
  \end{itemize}
  \vskip5mm
  \structure{Issues}
  \begin{itemize}
    \item The tree is possibly infinite is size
    \item Even with finite trees, it would be computationally infeasible
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Artificial Intelligence}
  \structure{Proposed Algorithm}
  \begin{itemize}
    \item Instead of going all the way down to the ``bottom'' of the tree, limit yourself to $N$ levels down, e.g.\ $N = 5$.
  \end{itemize}
  \vskip5mm
  \structure{Issues}
  \begin{itemize}
    \item There might be no win/loss states within $N$ moves
    \item Picking between moves with 0\% win and 0\% win is equivalent with playing randomly
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Heuristic}
  \begin{itemize}
    \item We need a way to determine how ``good'' a board state is without looking at all possible future moves
    \item Why not try to give a score to a board?
    \item The score must somehow try to predict the win\%
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Heuristic: Chess Example}  
  \begin{itemize}
    \item Let's develop a heuristic for chess
    \item The heuristic should express how good a board is for white
    \item High score = white will probably win
    \item Low score = white will probably lose
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Heuristic: Chess Example}  
  \begin{overprint}
    \onslide<1>
    \begin{center}
      \begin{tikzpicture}[scale=.7,transform shape]
        \node at (-4,0) {
          \newgame
          \fenboard{rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1}
          \showboard
        };

        \node[font=\Huge] at (0,0) {$>$};

        \node at (4,0) {
          \newgame
          \fenboard{rnbqkbnr/pppppppp/8/8/8/8/8/1111K111 w KQkq - 0 1}
          \showboard
        };
      \end{tikzpicture}
    \end{center}

    \onslide<2>
    \begin{center}
      \begin{tikzpicture}[scale=.7,transform shape]
        \node at (-4,0) {
          \newgame
          \fenboard{1111k111/8/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1}
          \showboard
        };

        \node[font=\Huge] at (0,0) {$>$};

        \node at (4,0) {
          \newgame
          \fenboard{rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1}
          \showboard
        };
      \end{tikzpicture}
    \end{center}

    \onslide<3>
    \begin{center}
      \begin{tikzpicture}[scale=.7,transform shape]
        \node at (-4,0) {
          \newgame
          \fenboard{rnbqkbnr/pppppppp/8/8/8/8/PPP1PPPP/RNBQKBNR w KQkq - 0 1}
          \showboard
        };

        \node[font=\Huge] at (0,0) {$>$};

        \node at (4,0) {
          \newgame
          \fenboard{rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNB1KBNR w KQkq - 0 1}
          \showboard
        };
      \end{tikzpicture}
    \end{center}

    \onslide<4>
    \begin{center}
      \begin{tikzpicture}[scale=.7,transform shape]
        \node at (-4,0) {
          \newgame
          \fenboard{rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1}
          \showboard
        };

        \node[font=\Huge] at (0,0) {$>$};

        \node at (4,0) {
          \newgame
          \fenboard{rnbqkbnr/pppppppp/8/4K3/8/8/PPPPPPPP/RNBQ1BNR w KQkq - 0 1}
          \showboard
        };
      \end{tikzpicture}
    \end{center}

  \end{overprint}

  \vskip5mm

  \begin{overprint}
    \onslide<1>
    \begin{center}
      First attempt: count number of white pieces
    \end{center}

    \onslide<2>
    \begin{center}
      Refinement: \#whites - \#blacks
    \end{center}

    \onslide<3>
    \begin{center}
      Weights should be introduced: e.g.\ pawns=1, queen=8
    \end{center}

    \onslide<4>
    \begin{center}
      Heuristic should also reflect whether king is in danger
    \end{center}
  \end{overprint}
\end{frame}

\begin{frame}
  \frametitle{Heuristic}
  \begin{itemize}
    \item Finding good heuristic is very hard
    \item Heuristic can reflect difficulty
    \item Easy: simple heuristic
    \item Medium: smarter heuristic
    \item Hard: really smart heuristic
    \item Determining heuristic quality can be achieved by simulating tournaments: AI vs AI
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Artificial Intelligence}
  \structure{In our approach\dots}
  \begin{itemize}
    \item Quality of AI is determined by
          \begin{itemize}
            \item ``Ply'': how many moves ahead does the AI consider?
            \item Heuristic
            \item Search algorithm used (not discussed)
          \end{itemize}
    \item We provide several search algorithms
          \begin{itemize}
            \item Random (dumb)
            \item Direct (medium)
            \item Minimax (smartest)
            \item Minimax with $\alpha$-$\beta$-pruning (smartest, but also faster)
          \end{itemize}
  \end{itemize}  
\end{frame}

\begin{frame}
  \frametitle{Code Examples: Creating AI}
  \structure{Create Heuristic}
  \code[font size=\tiny,width=.95\linewidth]{create-heuristic.cs}

  \structure{Create AI}
  \code[font size=\tiny,width=.95\linewidth]{create.cs}

  \structure{Ply}
  \begin{itemize}
    \item Ply = number of moves the algorithm looks into the future
    \item Higher ply = slower performance
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Code Examples: Using AI}
  \code[width=.95\linewidth]{usage.cs}
  \begin{itemize}
    \item AI requires {\tt Game}-object
    \item It will return ``best'' move for current player
  \end{itemize}
\end{frame}


\end{document}



%%% Local Variables: 
%%% mode: latex
%%% TeX-master: t
%%% End: 
