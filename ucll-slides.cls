\NeedsTeXFormat{LaTeX2e}
\ProvidesPackage{khlslides}[16/05/2014]
\LoadClass{beamer}

\usepackage{graphicx}
\usepackage{pxfonts}
\usepackage{tikz}
\usepackage{calc}
\usepackage{listings}
\usepackage{booktabs}
\usepackage{url}
\usepackage{framed}
\usepackage{hyperref}
\usepackage{cleveref}

\usetikzlibrary{shadows,calc,shapes}


\mode<presentation>
\usetheme[height=.75cm]{Rochester}
% \setbeamertemplate{background canvas}[vertical shading][top=blue!10,bottom=blue!30]

\institute[UCLL]{UC Leuven Limburg}
% \logo{\includegraphics[height=0.5cm]{../ucll-logo.png}}


% Code formatting
\pgfkeys{
  /khl/code/.cd,
  frame/.code=\lstset{frame=#1},
  font size/.code=\lstset{basicstyle={\ttfamily #1}},
  width/.initial=.8\linewidth,
  language/.code=\lstset{language=#1},
  show lines/.code=\lstset{showlines}
}

% Styles
\pgfkeys{
  /khl/note/.style={drop shadow,fill=blue!60,opacity=.8,text opacity=1},
  /khl/note arrow/.style={blue!60,ultra thick,-latex,opacity=.8}
}


\lstdefinelanguage{csharp}[Sharp]{C}{
  morekeywords={var,partial},
  escapeinside=\`\`,
  keywordstyle=\color{blue}\bfseries,
  ndkeywords={class, export, boolean, throw, implements, import, this},
  ndkeywordstyle=\color{darkgray}\bfseries,
  identifierstyle=\color{black},
  sensitive=false,
  comment=[l]{//},
  morecomment=[s]{/*}{*/},
  commentstyle=\color{purple}\ttfamily,
  stringstyle=\color{black}\ttfamily,
  basicstyle=\tt,
  morestring=[b]',
  morestring=[b]",
  escapeinside=\`\`,
  showstringspaces=false,
  frame=lines
}




\lstset{language=csharp}

% \code[pgfkeys]{filename}
\newcommand{\code}[2][]{
  {
    \pgfkeys{/khl/code/.cd,#1}
    \begin{center}
      \pgfkeys{/khl/code/width/.get=\width}
      \begin{minipage}{\width}
        \lstinputlisting{#2}
      \end{minipage}
    \end{center}
  }
}

\newcommand{\inlinecode}[2][]{
  {
    \pgfkeys{/khl/code/.cd,#1}
    \pgfkeys{/khl/code/width/.get=\width}
    \begin{minipage}{\width}
      \lstinputlisting{#2}
    \end{minipage}
  }
}


% Tikz related commands
% \NODE{text}{id}
\newcommand{\NODE}[3][]{\tikz[baseline,remember picture]{\node[anchor=base,inner sep=0mm,#1] (#3) {{#2}};}}
\newcommand{\COORD}[1]{\tikz[overlay,baseline,remember picture]{\coordinate (#1);}}


\newcommand{\toc}{
  \begin{frame}
    \tableofcontents[sectionstyle=show/shaded,subsectionstyle=show/shaded/hide]
  \end{frame}
}


\setbeamertemplate{title page}{%
  \begin{center}
    VGO \\[5mm]
    {\sc\Huge \inserttitle}
  \end{center}
}

\newcommand{\link}[2]{\href{#1}{{\color{blue}#2}}}

\definecolor{darkgreen}{RGB}{0,127,0}