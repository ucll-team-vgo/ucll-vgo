import javax.swing.Timer; // + others

public static void main(String[] args) {
  SwingUtilities.invokeLater( new Runnable() {
    @Override public void run() {
      JFrame frame = new JFrame();
      frame.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );
      final JLabel lbl = new JLabel();
      frame.add( lbl );
      frame.setVisible( true );
      new Timer( 500, new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          String s = new SimpleDateFormat( "HH:mm:ss" )
              .format( Calendar.getInstance().getTime() );
          lbl.setText( s );
        }
      } ).start();
    }
  } );
}
