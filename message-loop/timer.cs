public partial class MainWindow : Window {
  public MainWindow() {
    InitializeComponent();

    var tb = new TextBlock();
    this.AddChild( tb );

    new DispatcherTimer(
      priority: DispatcherPriority.ApplicationIdle,
      dispatcher: this.Dispatcher,
      interval: TimeSpan.FromMilliseconds(500),
      callback: (s, e) =>
         tb.Text = DateTime.Now.ToLongTimeString()
    ).Start();
  }
}