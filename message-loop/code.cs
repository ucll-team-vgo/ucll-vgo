`\COORD{init}`var isPrime = true;

for ( var `\COORD{i init}`i = 2; `\COORD{i cmp}`i < n; `\COORD{i inc}`++i )
{
    `\COORD{divisible}` var divisible = n % i;

    `\COORD{if div}`if ( divisible )
        `\COORD{no prime}`isPrime = false;
}

`\COORD{if prime}`if ( isPrime )
    `\COORD{print prime}`Console.WriteLine("{0} is a prime", n);
else
    Console.WriteLine("{0} is not a prime", n);