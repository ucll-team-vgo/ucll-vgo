public partial class MainWindow : Window {
    public MainWindow() {
        InitializeComponent();

        var thread = new Thread( ComputePrimes );
        thread.IsBackground = true;
        thread.Start();
    }

    private void ComputePrimes() {
        for ( int i = 2; i < 1000000; ++i ) {
            if ( IsPrime(i) ) {
                this.Dispatcher.InvokeAsync( () => 
                   listbox.Items.Add( n ) );
            }
        }
    }

    private bool IsPrime(int n) { ... }
}