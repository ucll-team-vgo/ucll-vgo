var threads = new List<Thread>();
var count = 0;

for ( var i = 0; i != `\NODE{10}{nthreads}`; ++i ) {
    var thread = new `\NODE{Thread}{creation}`( delegate() { 
            for ( var j = 0; j != `\NODE{100000}{increments}`; ++j ) {
                ++count;
            }
        } );
    threads.Add( thread );                
}

// Start each thread
foreach ( var thread in threads ) thread.Start();

// Wait for each thread to finish
foreach ( var thread in threads ) thread.Join();

Console.WriteLine( `\NODE{count}{output}` );
