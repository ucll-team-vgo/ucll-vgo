public MainWindow()
{
    InitializeComponent();

    // Tells the GUI library to call ButtonPressed
    // when the button control receives WM_COMMAND
    button.Click += ButtonPressed;
}

// Called by the GUI library
private void ButtonPressed( object sender,
                            RoutedEventArgs e )
{
    MessageBox.Show( "Button pressed!" );
}