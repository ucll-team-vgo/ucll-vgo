require 'MetaData'
require 'Image'
require 'LaTeX'
require 'Upload'


class Context
  include Contracts::TypeChecking
end


meta_object do
  extend MetaData::Actions
  extend LaTeX::Actions
  extend Upload::Mixin

  def remote_directory
    world.parent.remote_directory + 'message-loop'
  end

  tex_files = tex_actions('message-loop.tex', group_name: :tex)

  uploadable('message-loop.pdf')
  upload_action

  group_action(:full, [:tex, :upload])
end
