// ...
import java.util.Timer;

public static void main(String[] args) {
    JFrame frame = new JFrame();
    frame.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );
    final JLabel lbl = new JLabel();
    frame.add(lbl);
    frame.setVisible( true );
    
    Timer timer = new Timer(true);
    timer.scheduleAtFixedRate( new TimerTask() {
        @Override
        public void run() {
            String s = new SimpleDateFormat("HH:mm:ss")
               .format( Calendar.getInstance().getTime() );
            lbl.setText( s );
        }
    }, 0, 500);        
}
