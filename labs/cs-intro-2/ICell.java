public interface ICell<T>
{
    public T read();

    public void write(T value);

    public void addObserver(ICellObserver<T> observer);
}
