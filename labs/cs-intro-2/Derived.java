public class Derived<T, R> extends Cell<R>
{
    private final ITransformer<T, R> transformer;
    
    protected Derived( ICell<T> cell, ITransformer<T, R> transformer )
    {
        super( transformer.transform( cell.read() ) );
        
        this.transformer = transformer;
        
        derived.addObserver( new Observer() );
    }
    
    private class Observer implements ICellObserver<T>
    {
        @Override
        public void update(ICell<T> cell)
        {
            Derived.super.write( transformer.transform(cell.read()) );
        }       
    }
    
    @Override
    public void write(R value)
    {
        throw new UnsupportedOperationException();
    }
}
