private void NotifyObservers()
{
    if ( ValueChanged != null )
    {
        ValueChanged( this );
    }
}
