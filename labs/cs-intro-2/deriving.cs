var n = new Cell<int>( 4 );
var isEven = new Derived<int, bool>( n, x => x % 2 == 0 );
