public class Cell<T> implements ICell<T>
{
    private T value;
    private List<ICellObserver<T>> observers;

    protected Cell( T initialValue )
    {
        this.value = initialValue;
        this.observers = new ArrayList<ICellObserver<T>>();
    }

    @Override
    public void addObserver(ICellObserver<T> observer)
    {
        observers.add( observer );
    }
    
    @Override
    public T read()
    {
        return this.value;
    }
    
    @Override
    public void write(T value)
    {
        if ( !areEqual( this.value, value ) )
        {
            this.value = value;
            notifyObservers();
        }
    }

    private boolean areEqual(T x, T y)
    {
        if ( x == null )
        {
            return y == null;
        }
        else
        {
            return x.equals( y );
        }
    }    

    private void notifyObservers()
    {
        for ( ICellObserver<T> observer : observers )
        {
            observer.update( this );
        }
    }
}
