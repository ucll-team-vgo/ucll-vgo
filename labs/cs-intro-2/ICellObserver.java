public interface ICellObserver<T>
{
    public void update(ICell<T> variable);
}
