require 'MetaData'
require 'Image'
require 'Html'
require 'LaTeX'
require 'Code'
require 'Upload'


class Context
  include Contracts::TypeChecking

  def code(filename)
    typecheck do
      assert(filename: string)
    end

    Code.format_file(Pathname.new filename)
  end
end


meta_object do
  extend MetaData::Actions
  extend Html::Actions
  extend LaTeX::Actions
  extend Image::Actions
  extend Upload::Mixin

  def remote_directory
    world.parent.remote_directory + 'cs-intro-2'
  end

  tex_files = auto_tex(group_name: :tex)
  pdf_files = tex_files.map { |tex| tex.sub_ext '.pdf' }
  png_files = tex_files.map { |tex| tex.sub_ext '.png' }
  pdf_to_png(*pdf_files.map { |pdf| pdf.basename('.pdf').to_s }, group_name: :png)

  group_action(:images, [ :tex, :png ])
  
  html_template('assignment', group_name: 'html', context: Context.new)
  
  uploadable('assignment.html')
  uploadable( *Dir['*.png'] )
  uploadable( *Dir['*.cs'] )
  uploadable( *Dir['*.java'] )
  upload_action

  group_action(:full, [:html, :images, :upload])
end
