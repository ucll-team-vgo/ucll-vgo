public interface ITransformer<IN, OUT>
{
    public OUT transform(IN in);
}
