
ICell<Integer> n = new Cell<Integer>( 4 );
ICell<Boolean> isEven = new Derived<Integer, Boolean>( n, new ITransformer<Integer, Boolean>()
{
    @Override
    public Boolean transform(Integer n)
    {
        return n % 2 == 0;
    }
} );
