private int n;

public void initialize()
{
    slider.addObserver( new SliderObserver() {
                            @Override public void updateReceived(int newSliderValue)
                            {
                                n = newSliderValue;
                            }
                        } );
}
