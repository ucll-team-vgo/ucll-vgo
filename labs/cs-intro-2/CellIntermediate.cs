using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Solution
{
    public interface ICell<T>
    {
        T Value { get; set; }

        event Action<ICell<T>> ValueChanged;
    }

    public abstract class Cell<T> : ICell<T>
    {
        private T value;

        public event Action<ICell<T>> ValueChanged;

        protected Cell( T initialValue )
        {
            this.value = initialValue;
        }

        public T Value
        {
            get
            {
                return this.value;
            }
            set
            {
                if ( !AreEqual( this.value, value ) )
                {
                    this.value = value;
                    NotifyObservers();
                }
            }
        }

        private bool AreEqual( T x, T y )
        {
            if ( x == null )
            {
                return y == null;
            }
            else
            {
                return x.Equals( y );
            }
        }

        private void NotifyObservers()
        {
            if ( ValueChanged != null )
            {
                ValueChanged( this );
            }
        }
    }
}
