private int n;

public void initialize()
{
    slider.addObserver( new SliderObserver() {
                            @Override public void updateReceived(int newSliderValue)
                            {
                                n = newSliderValue;
                            }
                        } );
}

public void setN(int newValue)
{
    n = newValue;
    slider.setValue(newValue);
}
