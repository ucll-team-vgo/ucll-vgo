using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Solution
{
    public class Vector2D
    {
        private int x;
        private int y;

        public Vector2D(int x = 0, int y = 0)
        {
            this.x = x;
            this.y = y;
        }

        public int X
        {
            get
            {
                return x;
            }
            set
            {
                this.x = value;
            }
        }

        public int Y
        {
            get
            {
                return y;
            }
            set
            {
                this.y = value;
            }
        }

        public static Vector2D operator +(Vector2D u, Vector2D v)
        {
            var x = u.X + v.X;
            var y = u.Y + v.Y;

            return new Vector2D( x, y );
        }

        public static Vector2D operator *(Vector2D v, int factor)
        {
            var x = v.X * factor;
            var y = v.Y * factor;

            return new Vector2D( x, y );
        }

        public override string ToString()
        {
            return string.Format( "({0}, {1})", x, y );
        }

        public override bool Equals( object obj )
        {
            return Equals( obj as Vector2D );
        }

        public bool Equals(Vector2D v)
        {
            return v != null && x == v.x && y == v.y;
        }

        public override int GetHashCode()
        {
            return x ^ y;
        }

        public static IEnumerable<Vector2D> Directions
        {
            get
            {
                for ( var dx = -1; dx <= 1; ++dx )
                {
                    for ( var dy = -1; dy <= 1; ++dy )
                    {
                        if ( dx != 0 || dy != 0 )
                        {
                            yield return new Vector2D( dx, dy );
                        }
                    }
                }
            }
        }

        public IEnumerable<Vector2D> Around
        {
            get
            {
                return from direction in Directions
                       select this + direction;
            }
        }
    }
}
