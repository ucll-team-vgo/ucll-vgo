require 'MetaData'
require 'Image'
require 'Html'
require 'Upload'


class Context
  include Contracts::TypeChecking
end


meta_object do
  extend MetaData::Actions
  extend Html::Actions
  extend Upload::Mixin

  def remote_directory
    world.parent.remote_directory + 'cs-intro-1'
  end

  html_template('assignment', group_name: 'html', context: Context.new)
  
  uploadable('assignment.html', 'Vector2D.cs', 'Vector2D.java', 'dragon.png')
  upload_action

  group_action(:full, [:html, :upload])
end
