require 'MetaData'
require 'Image'
require 'Html'
require 'LaTeX'
require 'Code'
require 'Upload'


def generate_dice
  (1..6).each do |value|
    base = "die#{value}"
    tex = "#{base}.tex"


    File.open(tex, "w") do |out|
      out.write(<<-'END'.gsub(/N/, value.to_s))
        \documentclass{standalone}
        \usepackage{epsdice}

        \begin{document}
        \epsdice{N}
        \end{document}
      END
    end
  end
end


class Context
  include Contracts::TypeChecking

  def code(filename)
    typecheck do
      assert(filename: string)
    end

    Code.format_file(Pathname.new filename).strip
  end
end


meta_object do
  extend MetaData::Actions
  extend Html::Actions
  extend LaTeX::Actions
  extend Image::Actions
  extend Upload::Mixin

  def remote_directory
    world.parent.remote_directory + 'yahtzee'
  end

  action(:dice, description: 'Generates .tex files for dice') do
    (1..6).each do |value|
      base = "die#{value}"
      tex = "#{base}.tex"
      
      File.open(tex, "w") do |out|
        out.write(<<-'END'.gsub(/N/, value.to_s))
        \documentclass{standalone}
        \usepackage{epsdice}

        \begin{document}
        \epsdice{N}
        \end{document}
      END
      end
    end
  end

  
  tex_files = auto_tex(allow_multiple: true, group_name: :tex)
  pdf_files = tex_files.map { |tex| tex.sub_ext '.pdf' }
  png_files = tex_files.map { |tex| tex.sub_ext '.png' }
  pdf_to_png(*pdf_files.map { |pdf| pdf.basename('.pdf').to_s }, group_name: :png)

  group_action(:images, [ :dice, :tex, :png ])
  
  html_template('assignment', group_name: 'html', context: Context.new)
  
  uploadable('assignment.html', 'assignment.css')
  uploadable( *Dir['*.png'] )
  upload_action

  group_action(:full, [:html, :images, :upload])
end
