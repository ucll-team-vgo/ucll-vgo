// Create a new ScoreSheet
var scoreSheet = new ScoreSheet();

// Grab the first ScoreLine (corresponds to "Ones")
var firstLine = scoreSheet.ScoreLines[0];

// Fake a dice roll
var roll = new DiceRoll( 1, 1, 2, 2, 3 );

// Assign the roll to the first line
firstLine.Assign(roll);

// Asks how much the player received (will be 2)
var score = firstLine.Score.Value;
