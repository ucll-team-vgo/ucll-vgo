// Call CanExecuteChanged whenever CanRoll changes
viewModel.CanRoll.PropertyChanged += ( sender, args ) =>
{
    if ( CanExecuteChanged != null )
    {
        CanExecuteChanged( this, new EventArgs() );
    }
};