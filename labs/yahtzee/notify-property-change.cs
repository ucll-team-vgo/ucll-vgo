private void NotifyPropertyChange(string property)
{
    if ( PropertyChanged != null )
    {
        PropertyChanged( this, new PropertyChangedEventArgs( property ) );
    }
}