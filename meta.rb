require 'MetaData'
require 'Upload'


meta_object do
  extend MetaData::Actions

  def remote_directory
    Pathname.new '/var/www/courses/vgo'
  end
end
